/// to measure GPU usage: https://askubuntu.com/questions/387594/how-to-measure-gpu-usage
/// -> sudo radeontop

// TODO share benchmarks with https://github.com/PistonDevelopers/imageproc/issues/98 ?

// TODO benchmark that includes loading texture to GPU, loading image (the complete process)

extern crate criterion;
#[macro_use]
extern crate glium;
extern crate image;

use criterion::{Bencher, Criterion};

use std::io::Cursor;

use image::imageops::filter3x3;
use image::GenericImage;
use glium::{DisplayBuild, Surface};
use glium::backend::Facade;

fn main() {
    let img = image::load(Cursor::new(&include_bytes!("../../resources/pigeons.png")[..]),
                          image::PNG)
                  .unwrap();
    let (width, height) = img.dimensions();

    let filter_imageproc = |b: &mut Bencher| {
        b.iter_with_large_drop(|| filter3x3(&img, &[0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]));
    };

    let filter_imageproc_drop = |b: &mut Bencher| {
        b.iter(|| filter3x3(&img, &[0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9]));
    };

    let filter_imageproc_with_image_loading = |b: &mut Bencher| {
        b.iter(|| {
            let img = image::load(Cursor::new(&include_bytes!("../../resources/pigeons.png")[..]),
                                  image::PNG)
                          .unwrap();
            filter3x3(&img, &[0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9])
        });
    };

    let image = img.to_rgba();
    let image_dimensions = image.dimensions();

    let display = glium::glutin::WindowBuilder::new()
                      .with_visibility(false)
                      .build_glium()
                      .unwrap();

    let image = glium::texture::RawImage2d::from_raw_rgba_reversed(image.into_raw(),
                                                                   image_dimensions);
    let texture =
        glium::texture::SrgbTexture2d::with_mipmaps(&display,
                                                    image,
                                                    glium::texture::MipmapsOption::NoMipmap)
            .unwrap();

    let output = glium::texture::SrgbTexture2d::empty(&display,
                                                      image_dimensions.0,
                                                      image_dimensions.1)
                     .unwrap();

    #[derive(Copy, Clone)]
    struct Vertex {
        position: [f32; 2],
        tex_coords: [f32; 2],
    }

    implement_vertex!(Vertex, position, tex_coords);

    let shape = {
        let vertex1 = Vertex {
            position: [-1.0, -1.0],
            tex_coords: [0.0, 0.0],
        };
        let vertex2 = Vertex {
            position: [-1.0, 1.0],
            tex_coords: [0.0, 1.0],
        };
        let vertex3 = Vertex {
            position: [1.0, -1.0],
            tex_coords: [1.0, 0.0],
        };
        let vertex4 = Vertex {
            position: [1.0, 1.0],
            tex_coords: [1.0, 1.0],
        };

        vec![vertex1, vertex2, vertex3, vertex4]
    };

    let vertex_buffer = glium::VertexBuffer::new(&display, &shape).unwrap();
    let indices = glium::index::NoIndices(glium::index::PrimitiveType::TriangleStrip);

    let program = {
        let vertex_shader_src = r#"
            #version 140

            in vec2 position;
            in vec2 tex_coords;
            out vec2 v_tex_coords;

            void main() {
                v_tex_coords = tex_coords;
                gl_Position = vec4(position, 0.0, 1.0);
            }
        "#;

        // http://r3dux.org/2011/06/glsl-image-processing/
        let fragment_shader_src = r#"
            #version 140
            in vec2 v_tex_coords;
            out vec4 color;

            uniform sampler2D tex;
            uniform float step_x;
            uniform float step_y;

            void main() {
                vec2 sx = vec2(step_x, 0.0);
                vec2 sy = vec2(step_y, 0.0);
                vec2 c = v_tex_coords;
                color =
                    0.1 * texture(tex, c - sx + sy) + 0.2 * texture(tex, c + sy) + 0.3 * texture(tex, c + sx + sy) +
                    0.4 * texture(tex, c - sx)      + 0.5 * texture(tex, c)      + 0.6 * texture(tex, c + sx)      +
                    0.7 * texture(tex, c - sx - sy) + 0.8 * texture(tex, c - sy) + 0.9 * texture(tex, c + sx - sy);
            }
        "#;

        glium::Program::from_source(&display, vertex_shader_src, fragment_shader_src, None).unwrap()
    };

    let program_offset_lookup = {
        let vertex_shader_src = r#"
            #version 140

            in vec2 position;
            in vec2 tex_coords;
            out vec2 v_tex_coords;

            void main() {
                v_tex_coords = tex_coords;
                gl_Position = vec4(position, 0.0, 1.0);
            }
        "#;

        // http://r3dux.org/2011/06/glsl-image-processing/
        let fragment_shader_src = r#"
            #version 140
            in vec2 v_tex_coords;
            out vec4 color;

            uniform sampler2D tex;

            void main() {
                vec2 c = v_tex_coords;
                color =
                    0.1 * textureOffset(tex, c, ivec2(-1, 1))  + 0.2 * textureOffset(tex, c, ivec2(0, 1))  + 0.3 * textureOffset(tex, c, ivec2(1, 1)) +
                    0.4 * textureOffset(tex, c, ivec2(-1, 0))  + 0.5 * textureOffset(tex, c, ivec2(0, 0))  + 0.6 * textureOffset(tex, c, ivec2(1, 0)) +
                    0.7 * textureOffset(tex, c, ivec2(-1, -1)) + 0.8 * textureOffset(tex, c, ivec2(0, -1)) + 0.9 * textureOffset(tex, c, ivec2(1, -1));
            }
        "#;

        glium::Program::from_source(&display, vertex_shader_src, fragment_shader_src, None).unwrap()
    };

    let program_coords_in_vert = {
        let vertex_shader_src = r#"
            #version 140

            in vec2 position;
            in vec2 tex_coords;
            out vec2 v_tex_coords[9];

            uniform float step_x;
            uniform float step_y;

            void main() {
                vec2 c = tex_coords;
                vec2 sx = vec2(step_x, 0.0);
                vec2 sy = vec2(step_y, 0.0);
                v_tex_coords[0] = c - sx + sy;
                v_tex_coords[1] = c + sy;
                v_tex_coords[2] = c + sx + sy;
                v_tex_coords[3] = c - sx;
                v_tex_coords[4] = c;
                v_tex_coords[5] = c + sx;
                v_tex_coords[6] = c - sx - sy;
                v_tex_coords[7] = c - sy;
                v_tex_coords[8] = c + sx - sy;
                gl_Position = vec4(position, 0.0, 1.0);
            }
        "#;

        // http://r3dux.org/2011/06/glsl-image-processing/
        let fragment_shader_src = r#"
            #version 140
            in vec2 v_tex_coords[9];
            out vec4 color;

            uniform sampler2D tex;

            void main() {
                color =
                    0.1 * texture(tex, v_tex_coords[0]) + 0.2 * texture(tex, v_tex_coords[1]) + 0.3 * texture(tex, v_tex_coords[2]) +
                    0.4 * texture(tex, v_tex_coords[3]) + 0.5 * texture(tex, v_tex_coords[4]) + 0.6 * texture(tex, v_tex_coords[5]) +
                    0.7 * texture(tex, v_tex_coords[6]) + 0.8 * texture(tex, v_tex_coords[7]) + 0.9 * texture(tex, v_tex_coords[8]);
            }
        "#;

        glium::Program::from_source(&display, vertex_shader_src, fragment_shader_src, None).unwrap()
    };

    let uniforms = uniform! {
        tex: texture.sampled()
            .magnify_filter(glium::uniforms::MagnifySamplerFilter::Nearest)
            .minify_filter(glium::uniforms::MinifySamplerFilter::Nearest)
            .wrap_function(glium::uniforms::SamplerWrapFunction::Clamp),
        step_x: 1.0 / width as f32,
        step_y: 1.0 / height as f32
    };

    let uniforms_offset_lookup = uniform! {
        tex: texture.sampled()
            .magnify_filter(glium::uniforms::MagnifySamplerFilter::Nearest)
            .minify_filter(glium::uniforms::MinifySamplerFilter::Nearest)
            .wrap_function(glium::uniforms::SamplerWrapFunction::Clamp),
    };

    let filter_glium = |b: &mut Bencher| {
        let output = glium::texture::SrgbTexture2d::empty(&display,
                                                          image_dimensions.0,
                                                          image_dimensions.1)
                         .unwrap();
        let mut framebuffer = glium::framebuffer::SimpleFrameBuffer::new(&display, &output)
                                  .unwrap();
        b.iter(|| {
            framebuffer.draw(&vertex_buffer,
                             &indices,
                             &program,
                             &uniforms,
                             &Default::default())
                       .unwrap();
            // https://www.opengl.org/wiki/Synchronization
            display.get_context().finish();
        });
    };

    let filter_glium_offset_lookup = |b: &mut Bencher| {
        let output = glium::texture::SrgbTexture2d::empty(&display,
                                                          image_dimensions.0,
                                                          image_dimensions.1)
                         .unwrap();
        let mut framebuffer = glium::framebuffer::SimpleFrameBuffer::new(&display, &output)
                                  .unwrap();
        b.iter(|| {
            framebuffer.draw(&vertex_buffer,
                             &indices,
                             &program_offset_lookup,
                             &uniforms_offset_lookup,
                             &Default::default())
                       .unwrap();
            display.get_context().finish();
        });
    };

    let filter_glium_coords_in_vert = |b: &mut Bencher| {
        let output = glium::texture::SrgbTexture2d::empty(&display,
                                                          image_dimensions.0,
                                                          image_dimensions.1)
                         .unwrap();
        let mut framebuffer = glium::framebuffer::SimpleFrameBuffer::new(&display, &output)
                                  .unwrap();
        b.iter(|| {
            framebuffer.draw(&vertex_buffer,
                             &indices,
                             &program_coords_in_vert,
                             &uniforms,
                             &Default::default())
                       .unwrap();
            display.get_context().finish();
        });
    };

    let filter_glium_create_output_and_drop = |b: &mut Bencher| {
        b.iter(|| {
            let output = glium::texture::SrgbTexture2d::empty(&display,
                                                              image_dimensions.0,
                                                              image_dimensions.1)
                             .unwrap();
            let mut framebuffer = glium::framebuffer::SimpleFrameBuffer::new(&display, &output)
                                      .unwrap();
            framebuffer.draw(&vertex_buffer,
                             &indices,
                             &program,
                             &uniforms,
                             &Default::default())
                       .unwrap();
            display.get_context().finish();
        });
    };

    let filter_glium_with_image_loading = |b: &mut Bencher| {
        b.iter(|| {
            let img = image::load(Cursor::new(&include_bytes!("../../resources/pigeons.png")[..]),
                                  image::PNG).unwrap();
            let image = img.to_rgba();
            let image_dimensions = image.dimensions();
            let image = glium::texture::RawImage2d::from_raw_rgba_reversed(image.into_raw(), image_dimensions);
            let texture = glium::texture::SrgbTexture2d::with_mipmaps(&display, image, glium::texture::MipmapsOption::NoMipmap).unwrap();
            let output = glium::texture::SrgbTexture2d::empty(&display, image_dimensions.0, image_dimensions.1).unwrap();
            let mut framebuffer = glium::framebuffer::SimpleFrameBuffer::new(&display, &output).unwrap();
            let uniforms = uniform! {
                tex: texture.sampled()
                    .magnify_filter(glium::uniforms::MagnifySamplerFilter::Nearest)
                    .minify_filter(glium::uniforms::MinifySamplerFilter::Nearest)
                    .wrap_function(glium::uniforms::SamplerWrapFunction::Clamp),
                step_x: 1.0 / width as f32,
                step_y: 1.0 / height as f32
            };
            framebuffer.draw(&vertex_buffer, &indices, &program, &uniforms,
                &Default::default()).unwrap();
            display.get_context().finish();
        });
    };

    let filter_glium_with_image_load_and_download = |b: &mut Bencher| {
        b.iter(|| {
            let img = image::load(Cursor::new(&include_bytes!("../../resources/pigeons.png")[..]),
                                  image::PNG).unwrap();
            let image = img.to_rgba();
            let image_dimensions = image.dimensions();
            let image = glium::texture::RawImage2d::from_raw_rgba_reversed(image.into_raw(), image_dimensions);
            let texture = glium::texture::SrgbTexture2d::with_mipmaps(&display, image, glium::texture::MipmapsOption::NoMipmap).unwrap();
            let output = glium::texture::SrgbTexture2d::empty(&display, image_dimensions.0, image_dimensions.1).unwrap();
            let mut framebuffer = glium::framebuffer::SimpleFrameBuffer::new(&display, &output).unwrap();
            let uniforms = uniform! {
                tex: texture.sampled()
                    .magnify_filter(glium::uniforms::MagnifySamplerFilter::Nearest)
                    .minify_filter(glium::uniforms::MinifySamplerFilter::Nearest)
                    .wrap_function(glium::uniforms::SamplerWrapFunction::Clamp),
                step_x: 1.0 / width as f32,
                step_y: 1.0 / height as f32
            };
            framebuffer.draw(&vertex_buffer, &indices, &program, &uniforms,
                &Default::default()).unwrap();
            display.get_context().finish();
            let _: Vec<Vec<_>> = output.read();
        });
    };

    Criterion::default()
        .sample_size(20)
        .noise_threshold(0.05)
        .bench_function("filter_glium", filter_glium)
        .bench_function("filter_glium_offset_lookup", filter_glium_offset_lookup)
        .bench_function("filter_glium_coords_in_vert", filter_glium_coords_in_vert);

    /*Criterion::default()
        .sample_size(20)
        .noise_threshold(0.05)
        .bench_function("filter_imageproc", filter_imageproc)
        .bench_function("filter_imageproc_drop", filter_imageproc_drop)
        .bench_function("filter_glium", filter_glium)
        .bench_function("filter_glium_offset_lookup", filter_glium_offset_lookup)
        .bench_function("filter_glium_coords_in_vert", filter_glium_coords_in_vert)
        .bench_function("filter_glium_create_output_and_drop",
                        filter_glium_create_output_and_drop);

    Criterion::default()
        .sample_size(5)
        .noise_threshold(0.05)
        .bench_function("filter_imageproc_with_image_loading",
                        filter_imageproc_with_image_loading)
        .bench_function("filter_glium_with_image_loading",
                        filter_glium_with_image_loading)
        .bench_function("filter_glium_with_image_load_and_download",
                        filter_glium_with_image_load_and_download);*/
}
