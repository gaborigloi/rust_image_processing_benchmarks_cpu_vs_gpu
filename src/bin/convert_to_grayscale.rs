/// to measure GPU usage: https://askubuntu.com/questions/387594/how-to-measure-gpu-usage
/// -> sudo radeontop

// TODO share benchmarks with https://github.com/PistonDevelopers/imageproc/issues/98 ?

// TODO benchmark that includes loading texture to GPU, loading image (the complete process)

extern crate criterion;
#[macro_use]
extern crate glium;
extern crate image;

use criterion::{Bencher, Criterion};

use std::io::Cursor;

use image::imageops::grayscale;
use glium::{DisplayBuild, Surface};
use glium::backend::Facade;

fn main() {
    let img = image::load(Cursor::new(&include_bytes!("../../resources/pigeons.png")[..]),
                          image::PNG).unwrap();

    let imageproc_grayscale = |b: &mut Bencher| {
        b.iter_with_large_drop(|| {
            grayscale(&img)
        });
    };

    let imageproc_grayscale_drop = |b: &mut Bencher| {
        b.iter(|| {
            grayscale(&img)
        });
    };

    // modified code from glium examples: https://github.com/tomaka/glium
    // (https://github.com/tomaka/glium/blob/master/examples/tutorial-06.rs)
    // license: https://github.com/tomaka/glium/blob/master/LICENSE

    let image = img.to_rgba();
    let image_dimensions = image.dimensions();

    // returns BackendCreationError(NotSupported)
    /*let display = glium::glutin::HeadlessRendererBuilder::new(image.width(), image.height())
        .build_glium().unwrap();*/
    let display = glium::glutin::WindowBuilder::new()
        .with_visibility(false)
        .build_glium()
        .unwrap();

    let image = glium::texture::RawImage2d::from_raw_rgba_reversed(image.into_raw(), image_dimensions);
    let texture = glium::texture::SrgbTexture2d::with_mipmaps(&display, image, glium::texture::MipmapsOption::NoMipmap).unwrap();

    let output = glium::texture::SrgbTexture2d::empty(&display, image_dimensions.0, image_dimensions.1).unwrap();
    let mut framebuffer = glium::framebuffer::SimpleFrameBuffer::new(&display, &output).unwrap();

    #[derive(Copy, Clone)]
    struct Vertex {
        position: [f32; 2],
        tex_coords: [f32; 2],
    }

    implement_vertex!(Vertex, position, tex_coords);

    let shape = {
        let vertex1 = Vertex { position: [-1.0, -1.0], tex_coords: [0.0, 0.0] };
        let vertex2 = Vertex { position: [-1.0,  1.0], tex_coords: [0.0, 1.0] };
        let vertex3 = Vertex { position: [ 1.0, -1.0], tex_coords: [1.0, 0.0] };
        let vertex4 = Vertex { position: [ 1.0,  1.0], tex_coords: [1.0, 1.0] };

        vec![vertex1, vertex2, vertex3, vertex4]
    };

    let vertex_buffer = glium::VertexBuffer::new(&display, &shape).unwrap();
    let indices = glium::index::NoIndices(glium::index::PrimitiveType::TriangleStrip);

    let program = {
        let vertex_shader_src = r#"
            #version 140

            in vec2 position;
            in vec2 tex_coords;
            out vec2 v_tex_coords;

            void main() {
                v_tex_coords = tex_coords;
                gl_Position = vec4(position, 0.0, 1.0);
            }
        "#;

        // http://r3dux.org/2011/06/glsl-image-processing/
        let fragment_shader_src = r#"
            #version 140

            in vec2 v_tex_coords;
            out vec4 color;

            uniform sampler2D tex;

            void main() {
                // derive luminance from R, G, B co-ordinates
                float gray = dot(texture(tex, v_tex_coords).rgb, vec3(0.299, 0.587, 0.114));
                color = vec4(gray, gray, gray, 1.0);
            }
        "#;

        glium::Program::from_source(&display, vertex_shader_src, fragment_shader_src, None).unwrap()
    };

    let uniforms = uniform! {
        tex: texture.sampled()
            .magnify_filter(glium::uniforms::MagnifySamplerFilter::Nearest)
            .minify_filter(glium::uniforms::MinifySamplerFilter::Nearest)
            .wrap_function(glium::uniforms::SamplerWrapFunction::Clamp)
    };

    let glium_grayscale = |b: &mut Bencher| {
        b.iter(|| {
            framebuffer.draw(&vertex_buffer, &indices, &program, &uniforms,
                &Default::default()).unwrap();
            // https://www.opengl.org/wiki/Synchronization
            display.get_context().finish();
        });
    };

    let glium_grayscale_create_output_and_drop = |b: &mut Bencher| {
        b.iter(|| {
            let output = glium::texture::SrgbTexture2d::empty(&display, image_dimensions.0, image_dimensions.1).unwrap();
            let mut framebuffer = glium::framebuffer::SimpleFrameBuffer::new(&display, &output).unwrap();
            framebuffer.draw(&vertex_buffer, &indices, &program, &uniforms,
                &Default::default()).unwrap();
            display.get_context().finish();
        });
    };

    Criterion::default().noise_threshold(0.02)
        .bench_function("imageproc_grayscale", imageproc_grayscale)
        .bench_function("imageproc_grayscale_drop", imageproc_grayscale_drop)
        .bench_function("glium_grayscale", glium_grayscale)
        .bench_function("glium_grayscale_create_output_and_drop", glium_grayscale_create_output_and_drop);
}
